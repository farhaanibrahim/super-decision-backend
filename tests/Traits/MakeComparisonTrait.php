<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Comparison;
use App\Repositories\ComparisonRepository;

trait MakeComparisonTrait
{
    /**
     * Create fake instance of Comparison and save it in database
     *
     * @param array $comparisonFields
     * @return Comparison
     */
    public function makeComparison($comparisonFields = [])
    {
        /** @var ComparisonRepository $comparisonRepo */
        $comparisonRepo = \App::make(ComparisonRepository::class);
        $theme = $this->fakeComparisonData($comparisonFields);
        return $comparisonRepo->create($theme);
    }

    /**
     * Get fake instance of Comparison
     *
     * @param array $comparisonFields
     * @return Comparison
     */
    public function fakeComparison($comparisonFields = [])
    {
        return new Comparison($this->fakeComparisonData($comparisonFields));
    }

    /**
     * Get fake data of Comparison
     *
     * @param array $comparisonFields
     * @return array
     */
    public function fakeComparisonData($comparisonFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'decision_id' => $fake->randomDigitNotNull,
            'left' => $fake->word,
            'right' => $fake->word,
            'score' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $comparisonFields);
    }
}
