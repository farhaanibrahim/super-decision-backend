<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Respondent;
use App\Repositories\RespondentRepository;

trait MakeRespondentTrait
{
    /**
     * Create fake instance of Respondent and save it in database
     *
     * @param array $respondentFields
     * @return Respondent
     */
    public function makeRespondent($respondentFields = [])
    {
        /** @var RespondentRepository $respondentRepo */
        $respondentRepo = \App::make(RespondentRepository::class);
        $theme = $this->fakeRespondentData($respondentFields);
        return $respondentRepo->create($theme);
    }

    /**
     * Get fake instance of Respondent
     *
     * @param array $respondentFields
     * @return Respondent
     */
    public function fakeRespondent($respondentFields = [])
    {
        return new Respondent($this->fakeRespondentData($respondentFields));
    }

    /**
     * Get fake data of Respondent
     *
     * @param array $respondentFields
     * @return array
     */
    public function fakeRespondentData($respondentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'phone' => $fake->word,
            'respondent_type' => $fake->word,
            'traveling_budget' => $fake->word,
            'start_date' => $fake->word,
            'end_date' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $respondentFields);
    }
}
