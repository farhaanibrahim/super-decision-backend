<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Decision;
use App\Repositories\DecisionRepository;

trait MakeDecisionTrait
{
    /**
     * Create fake instance of Decision and save it in database
     *
     * @param array $decisionFields
     * @return Decision
     */
    public function makeDecision($decisionFields = [])
    {
        /** @var DecisionRepository $decisionRepo */
        $decisionRepo = \App::make(DecisionRepository::class);
        $theme = $this->fakeDecisionData($decisionFields);
        return $decisionRepo->create($theme);
    }

    /**
     * Get fake instance of Decision
     *
     * @param array $decisionFields
     * @return Decision
     */
    public function fakeDecision($decisionFields = [])
    {
        return new Decision($this->fakeDecisionData($decisionFields));
    }

    /**
     * Get fake data of Decision
     *
     * @param array $decisionFields
     * @return array
     */
    public function fakeDecisionData($decisionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'respondent_id' => $fake->randomDigitNotNull,
            'title' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $decisionFields);
    }
}
