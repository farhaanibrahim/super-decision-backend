<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\RespondentInfo;
use App\Repositories\RespondentInfoRepository;

trait MakeRespondentInfoTrait
{
    /**
     * Create fake instance of RespondentInfo and save it in database
     *
     * @param array $respondentInfoFields
     * @return RespondentInfo
     */
    public function makeRespondentInfo($respondentInfoFields = [])
    {
        /** @var RespondentInfoRepository $respondentInfoRepo */
        $respondentInfoRepo = \App::make(RespondentInfoRepository::class);
        $theme = $this->fakeRespondentInfoData($respondentInfoFields);
        return $respondentInfoRepo->create($theme);
    }

    /**
     * Get fake instance of RespondentInfo
     *
     * @param array $respondentInfoFields
     * @return RespondentInfo
     */
    public function fakeRespondentInfo($respondentInfoFields = [])
    {
        return new RespondentInfo($this->fakeRespondentInfoData($respondentInfoFields));
    }

    /**
     * Get fake data of RespondentInfo
     *
     * @param array $respondentInfoFields
     * @return array
     */
    public function fakeRespondentInfoData($respondentInfoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'respondent_id' => $fake->randomDigitNotNull,
            'gender' => $fake->word,
            'age' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $respondentInfoFields);
    }
}
