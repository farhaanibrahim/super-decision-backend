<?php namespace Tests\Repositories;

use App\Models\Comparison;
use App\Repositories\ComparisonRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeComparisonTrait;
use Tests\ApiTestTrait;

class ComparisonRepositoryTest extends TestCase
{
    use MakeComparisonTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ComparisonRepository
     */
    protected $comparisonRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->comparisonRepo = \App::make(ComparisonRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_comparison()
    {
        $comparison = $this->fakeComparisonData();
        $createdComparison = $this->comparisonRepo->create($comparison);
        $createdComparison = $createdComparison->toArray();
        $this->assertArrayHasKey('id', $createdComparison);
        $this->assertNotNull($createdComparison['id'], 'Created Comparison must have id specified');
        $this->assertNotNull(Comparison::find($createdComparison['id']), 'Comparison with given id must be in DB');
        $this->assertModelData($comparison, $createdComparison);
    }

    /**
     * @test read
     */
    public function test_read_comparison()
    {
        $comparison = $this->makeComparison();
        $dbComparison = $this->comparisonRepo->find($comparison->id);
        $dbComparison = $dbComparison->toArray();
        $this->assertModelData($comparison->toArray(), $dbComparison);
    }

    /**
     * @test update
     */
    public function test_update_comparison()
    {
        $comparison = $this->makeComparison();
        $fakeComparison = $this->fakeComparisonData();
        $updatedComparison = $this->comparisonRepo->update($fakeComparison, $comparison->id);
        $this->assertModelData($fakeComparison, $updatedComparison->toArray());
        $dbComparison = $this->comparisonRepo->find($comparison->id);
        $this->assertModelData($fakeComparison, $dbComparison->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_comparison()
    {
        $comparison = $this->makeComparison();
        $resp = $this->comparisonRepo->delete($comparison->id);
        $this->assertTrue($resp);
        $this->assertNull(Comparison::find($comparison->id), 'Comparison should not exist in DB');
    }
}
