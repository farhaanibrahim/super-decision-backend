<?php namespace Tests\Repositories;

use App\Models\Decision;
use App\Repositories\DecisionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeDecisionTrait;
use Tests\ApiTestTrait;

class DecisionRepositoryTest extends TestCase
{
    use MakeDecisionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var DecisionRepository
     */
    protected $decisionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->decisionRepo = \App::make(DecisionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_decision()
    {
        $decision = $this->fakeDecisionData();
        $createdDecision = $this->decisionRepo->create($decision);
        $createdDecision = $createdDecision->toArray();
        $this->assertArrayHasKey('id', $createdDecision);
        $this->assertNotNull($createdDecision['id'], 'Created Decision must have id specified');
        $this->assertNotNull(Decision::find($createdDecision['id']), 'Decision with given id must be in DB');
        $this->assertModelData($decision, $createdDecision);
    }

    /**
     * @test read
     */
    public function test_read_decision()
    {
        $decision = $this->makeDecision();
        $dbDecision = $this->decisionRepo->find($decision->id);
        $dbDecision = $dbDecision->toArray();
        $this->assertModelData($decision->toArray(), $dbDecision);
    }

    /**
     * @test update
     */
    public function test_update_decision()
    {
        $decision = $this->makeDecision();
        $fakeDecision = $this->fakeDecisionData();
        $updatedDecision = $this->decisionRepo->update($fakeDecision, $decision->id);
        $this->assertModelData($fakeDecision, $updatedDecision->toArray());
        $dbDecision = $this->decisionRepo->find($decision->id);
        $this->assertModelData($fakeDecision, $dbDecision->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_decision()
    {
        $decision = $this->makeDecision();
        $resp = $this->decisionRepo->delete($decision->id);
        $this->assertTrue($resp);
        $this->assertNull(Decision::find($decision->id), 'Decision should not exist in DB');
    }
}
