<?php namespace Tests\Repositories;

use App\Models\Respondent;
use App\Repositories\RespondentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRespondentTrait;
use Tests\ApiTestTrait;

class RespondentRepositoryTest extends TestCase
{
    use MakeRespondentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RespondentRepository
     */
    protected $respondentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->respondentRepo = \App::make(RespondentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_respondent()
    {
        $respondent = $this->fakeRespondentData();
        $createdRespondent = $this->respondentRepo->create($respondent);
        $createdRespondent = $createdRespondent->toArray();
        $this->assertArrayHasKey('id', $createdRespondent);
        $this->assertNotNull($createdRespondent['id'], 'Created Respondent must have id specified');
        $this->assertNotNull(Respondent::find($createdRespondent['id']), 'Respondent with given id must be in DB');
        $this->assertModelData($respondent, $createdRespondent);
    }

    /**
     * @test read
     */
    public function test_read_respondent()
    {
        $respondent = $this->makeRespondent();
        $dbRespondent = $this->respondentRepo->find($respondent->id);
        $dbRespondent = $dbRespondent->toArray();
        $this->assertModelData($respondent->toArray(), $dbRespondent);
    }

    /**
     * @test update
     */
    public function test_update_respondent()
    {
        $respondent = $this->makeRespondent();
        $fakeRespondent = $this->fakeRespondentData();
        $updatedRespondent = $this->respondentRepo->update($fakeRespondent, $respondent->id);
        $this->assertModelData($fakeRespondent, $updatedRespondent->toArray());
        $dbRespondent = $this->respondentRepo->find($respondent->id);
        $this->assertModelData($fakeRespondent, $dbRespondent->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_respondent()
    {
        $respondent = $this->makeRespondent();
        $resp = $this->respondentRepo->delete($respondent->id);
        $this->assertTrue($resp);
        $this->assertNull(Respondent::find($respondent->id), 'Respondent should not exist in DB');
    }
}
