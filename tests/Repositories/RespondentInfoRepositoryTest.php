<?php namespace Tests\Repositories;

use App\Models\RespondentInfo;
use App\Repositories\RespondentInfoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRespondentInfoTrait;
use Tests\ApiTestTrait;

class RespondentInfoRepositoryTest extends TestCase
{
    use MakeRespondentInfoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RespondentInfoRepository
     */
    protected $respondentInfoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->respondentInfoRepo = \App::make(RespondentInfoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_respondent_info()
    {
        $respondentInfo = $this->fakeRespondentInfoData();
        $createdRespondentInfo = $this->respondentInfoRepo->create($respondentInfo);
        $createdRespondentInfo = $createdRespondentInfo->toArray();
        $this->assertArrayHasKey('id', $createdRespondentInfo);
        $this->assertNotNull($createdRespondentInfo['id'], 'Created RespondentInfo must have id specified');
        $this->assertNotNull(RespondentInfo::find($createdRespondentInfo['id']), 'RespondentInfo with given id must be in DB');
        $this->assertModelData($respondentInfo, $createdRespondentInfo);
    }

    /**
     * @test read
     */
    public function test_read_respondent_info()
    {
        $respondentInfo = $this->makeRespondentInfo();
        $dbRespondentInfo = $this->respondentInfoRepo->find($respondentInfo->id);
        $dbRespondentInfo = $dbRespondentInfo->toArray();
        $this->assertModelData($respondentInfo->toArray(), $dbRespondentInfo);
    }

    /**
     * @test update
     */
    public function test_update_respondent_info()
    {
        $respondentInfo = $this->makeRespondentInfo();
        $fakeRespondentInfo = $this->fakeRespondentInfoData();
        $updatedRespondentInfo = $this->respondentInfoRepo->update($fakeRespondentInfo, $respondentInfo->id);
        $this->assertModelData($fakeRespondentInfo, $updatedRespondentInfo->toArray());
        $dbRespondentInfo = $this->respondentInfoRepo->find($respondentInfo->id);
        $this->assertModelData($fakeRespondentInfo, $dbRespondentInfo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_respondent_info()
    {
        $respondentInfo = $this->makeRespondentInfo();
        $resp = $this->respondentInfoRepo->delete($respondentInfo->id);
        $this->assertTrue($resp);
        $this->assertNull(RespondentInfo::find($respondentInfo->id), 'RespondentInfo should not exist in DB');
    }
}
