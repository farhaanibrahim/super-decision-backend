<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeDecisionTrait;
use Tests\ApiTestTrait;

class DecisionApiTest extends TestCase
{
    use MakeDecisionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_decision()
    {
        $decision = $this->fakeDecisionData();
        $this->response = $this->json('POST', '/api/decisions', $decision);

        $this->assertApiResponse($decision);
    }

    /**
     * @test
     */
    public function test_read_decision()
    {
        $decision = $this->makeDecision();
        $this->response = $this->json('GET', '/api/decisions/'.$decision->id);

        $this->assertApiResponse($decision->toArray());
    }

    /**
     * @test
     */
    public function test_update_decision()
    {
        $decision = $this->makeDecision();
        $editedDecision = $this->fakeDecisionData();

        $this->response = $this->json('PUT', '/api/decisions/'.$decision->id, $editedDecision);

        $this->assertApiResponse($editedDecision);
    }

    /**
     * @test
     */
    public function test_delete_decision()
    {
        $decision = $this->makeDecision();
        $this->response = $this->json('DELETE', '/api/decisions/'.$decision->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/decisions/'.$decision->id);

        $this->response->assertStatus(404);
    }
}
