<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRespondentTrait;
use Tests\ApiTestTrait;

class RespondentApiTest extends TestCase
{
    use MakeRespondentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_respondent()
    {
        $respondent = $this->fakeRespondentData();
        $this->response = $this->json('POST', '/api/respondents', $respondent);

        $this->assertApiResponse($respondent);
    }

    /**
     * @test
     */
    public function test_read_respondent()
    {
        $respondent = $this->makeRespondent();
        $this->response = $this->json('GET', '/api/respondents/'.$respondent->id);

        $this->assertApiResponse($respondent->toArray());
    }

    /**
     * @test
     */
    public function test_update_respondent()
    {
        $respondent = $this->makeRespondent();
        $editedRespondent = $this->fakeRespondentData();

        $this->response = $this->json('PUT', '/api/respondents/'.$respondent->id, $editedRespondent);

        $this->assertApiResponse($editedRespondent);
    }

    /**
     * @test
     */
    public function test_delete_respondent()
    {
        $respondent = $this->makeRespondent();
        $this->response = $this->json('DELETE', '/api/respondents/'.$respondent->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/respondents/'.$respondent->id);

        $this->response->assertStatus(404);
    }
}
