<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeComparisonTrait;
use Tests\ApiTestTrait;

class ComparisonApiTest extends TestCase
{
    use MakeComparisonTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_comparison()
    {
        $comparison = $this->fakeComparisonData();
        $this->response = $this->json('POST', '/api/comparisons', $comparison);

        $this->assertApiResponse($comparison);
    }

    /**
     * @test
     */
    public function test_read_comparison()
    {
        $comparison = $this->makeComparison();
        $this->response = $this->json('GET', '/api/comparisons/'.$comparison->id);

        $this->assertApiResponse($comparison->toArray());
    }

    /**
     * @test
     */
    public function test_update_comparison()
    {
        $comparison = $this->makeComparison();
        $editedComparison = $this->fakeComparisonData();

        $this->response = $this->json('PUT', '/api/comparisons/'.$comparison->id, $editedComparison);

        $this->assertApiResponse($editedComparison);
    }

    /**
     * @test
     */
    public function test_delete_comparison()
    {
        $comparison = $this->makeComparison();
        $this->response = $this->json('DELETE', '/api/comparisons/'.$comparison->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/comparisons/'.$comparison->id);

        $this->response->assertStatus(404);
    }
}
