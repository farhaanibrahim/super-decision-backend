<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeRespondentInfoTrait;
use Tests\ApiTestTrait;

class RespondentInfoApiTest extends TestCase
{
    use MakeRespondentInfoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_respondent_info()
    {
        $respondentInfo = $this->fakeRespondentInfoData();
        $this->response = $this->json('POST', '/api/respondentInfos', $respondentInfo);

        $this->assertApiResponse($respondentInfo);
    }

    /**
     * @test
     */
    public function test_read_respondent_info()
    {
        $respondentInfo = $this->makeRespondentInfo();
        $this->response = $this->json('GET', '/api/respondentInfos/'.$respondentInfo->id);

        $this->assertApiResponse($respondentInfo->toArray());
    }

    /**
     * @test
     */
    public function test_update_respondent_info()
    {
        $respondentInfo = $this->makeRespondentInfo();
        $editedRespondentInfo = $this->fakeRespondentInfoData();

        $this->response = $this->json('PUT', '/api/respondentInfos/'.$respondentInfo->id, $editedRespondentInfo);

        $this->assertApiResponse($editedRespondentInfo);
    }

    /**
     * @test
     */
    public function test_delete_respondent_info()
    {
        $respondentInfo = $this->makeRespondentInfo();
        $this->response = $this->json('DELETE', '/api/respondentInfos/'.$respondentInfo->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/respondentInfos/'.$respondentInfo->id);

        $this->response->assertStatus(404);
    }
}
