<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});


Auth::routes();

Route::group([
    'middleware' => 'auth'
], function ($router) {
    Route::get('/home', 'HomeController@index');
    Route::get('respondents/travelers', 'RespondentController@travelers')->name('respondents.travelers');
    Route::get('respondents/expertises', 'RespondentController@expertises')->name('respondents.expertises');
    Route::get('respondents/scores', 'RespondentController@scores')->name('respondents.scores');
    Route::get('decisions/scores/{key}', 'DecisionController@scores')->name('decisions.scores');
    Route::get('decisions/scores2/{key}', 'DecisionController@scores2')->name('decisions.scores2');

    Route::resource('respondents', 'RespondentController');
    Route::resource('respondentInfos', 'RespondentInfoController');
    Route::resource('decisions', 'DecisionController');
    Route::resource('comparisons', 'ComparisonController');
});
