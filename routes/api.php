<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('respondents/submit', 'RespondentAPIController@submit');
Route::get('respondents/check', 'RespondentAPIController@check');

Route::resource('respondents', 'RespondentAPIController');

Route::resource('respondent_infos', 'RespondentInfoAPIController');

Route::resource('decisions', 'DecisionAPIController');

Route::resource('comparisons', 'ComparisonAPIController');
