@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Respondent
        </h1>
    </section>
    <div class="content form-horizontal">
        @include('respondents.show_fields')
    </div>
@endsection
