{!! Form::hidden('respondent_type', "Expertise") !!}

<div class="row">
    <div class="col-md-12">
        <!-- Name Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('xid', 'Expertise ID:') !!}
            {!! Form::text('xid', $xid, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::select('city', ['Bandung' => 'Bandung', 'Yogyakarta' => 'Yogyakarta', 'Banyuwangi' => 'Banyuwangi', 'Bali' => 'Bali', 'Bali' => 'Bali', 'Malaka' => 'Malaka'], null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('respondents.expertises') !!}" class="btn btn-default">Cancel</a>
</div>
