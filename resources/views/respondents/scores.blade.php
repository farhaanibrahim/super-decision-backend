@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Expertises
        </h1>
    </section>
    <div class="content form-horizontal">
        <div class="box box-primary">
            <div class="box-body">
                <div class="box-header">
                    <h3 class="box-title">Scores</h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($titles as $k=>$title)
                            <tr>
                                <td>{{ $k + 1}}</td>
                                <td>{{ $title }}</td>
                                <td><a href="{{ route('decisions.scores', urlencode($title)) }}" class='btn btn-default btn-xs'>
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a> &nbsp;<a href="{{ route('decisions.scores2', urlencode($title)) }}" class='btn btn-success btn-xs'>
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                        </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
