<div class="box box-primary">
    <div class="box-body">
        <div class="box-header">
            <h3 class="box-title">Detail</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-md-6">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('name', $respondent->name, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('city', 'City', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('city', $respondent->city, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('phone', 'Phone', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('phone', $respondent->phone, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('respondent_type', 'Respondent Type', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('respondent_type', $respondent->respondent_type, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
                @if($respondent->respondent_type == 'Traveller')
                <div class="form-group col-md-6">
                    {!! Form::label('number_of_person', 'Person', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('number_of_person', $respondent->number_of_person, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('traveling_budget', 'Budget', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('traveling_budget', $respondent->formattedBudget(), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('duration', 'Duration', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('duration', $respondent->duration(), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('period', 'Period', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('period', $respondent->period(), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@if($respondent->respondent_type == 'Traveller')
<div class="box box-primary">
    <div class="box-body">
        <div class="box-header">
            <h3 class="box-title">Genders and Ages</h3>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Gender</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($respondent->respondentInfos as $k=>$info)
                    <tr>
                        <td>{{ $k + 1}}</td>
                        <td>{{ $info->gender }}</td>
                        <td>{{ $info->age}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif

<div class="box box-primary">
    <div class="box-body">
        <div class="box-header">
            <h3 class="box-title">Scores</h3>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($respondent->decisions as $k=>$decision)
                    <tr>
                        <td>{{ $k + 1}}</td>
                        <td>{{ $decision->title }}</td>
                        <td><a href="{{ route('decisions.show', $decision->id) }}" class='btn btn-default btn-xs'>
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-body">
        <div class="box-header">
            <h3 class="box-title">Timestamps</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Created At Field -->
                <div class="form-group col-md-6">
                    {!! Form::label('created_at', 'Created At', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('campaign_id', \Carbon\Carbon::parse($respondent->created_at)->format('d M Y H:i:s'), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>

                <!-- Updated At Field -->
                <div class="form-group col-md-6">
                    {!! Form::label('updated_at', 'Updated At', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::text('campaign_id', \Carbon\Carbon::parse($respondent->updated_at)->format('d M Y H:i:s'), ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
