@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Respondents</h1>
        <h1 class="pull-right">
            @if(Request::is('respondents/expertises*'))
            <a class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('respondents.create') !!}">Add New</a>&nbsp;
            <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('respondents.scores') !!}">Total Score</a>
            @endif
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('respondents.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

