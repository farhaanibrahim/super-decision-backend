@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Respondent Info
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($respondentInfo, ['route' => ['respondentInfos.update', $respondentInfo->id], 'method' => 'patch']) !!}

                        @include('respondent_infos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection