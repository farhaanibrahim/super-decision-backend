<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $respondentInfo->id !!}</p>
</div>

<!-- Respondent Id Field -->
<div class="form-group">
    {!! Form::label('respondent_id', 'Respondent Id:') !!}
    <p>{!! $respondentInfo->respondent_id !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $respondentInfo->gender !!}</p>
</div>

<!-- Age Field -->
<div class="form-group">
    {!! Form::label('age', 'Age:') !!}
    <p>{!! $respondentInfo->age !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $respondentInfo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $respondentInfo->updated_at !!}</p>
</div>

