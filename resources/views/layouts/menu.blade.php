<li class="{{ Request::is('respondents/travelers*') ? 'active' : '' }}">
    <a href="{!! route('respondents.travelers') !!}"><i class="fa fa-user"></i><span>Travellers</span></a>
</li>

<li class="{{ Request::is('respondents/expertises*') ? 'active' : '' }}">
    <a href="{!! route('respondents.expertises') !!}"><i class="fa fa-user-secret"></i><span>Expertise</span></a>
</li>

<!--
<li class="{{ Request::is('decisions*') ? 'active' : '' }}">
    <a href="{!! route('decisions.index') !!}"><i class="fa fa-edit"></i><span>Decisions</span></a>
</li>

<li class="{{ Request::is('comparisons*') ? 'active' : '' }}">
    <a href="{!! route('comparisons.index') !!}"><i class="fa fa-edit"></i><span>Comparisons</span></a>
</li>

<li class="{{ Request::is('respondents*') ? 'active' : '' }}">
    <a href="{!! route('respondents.index') !!}"><i class="fa fa-edit"></i><span>Respondents</span></a>
</li>

<li class="{{ Request::is('respondentInfos*') ? 'active' : '' }}">
    <a href="{!! route('respondentInfos.index') !!}"><i class="fa fa-edit"></i><span>Respondent Infos</span></a>
</li>

<li class="{{ Request::is('decisions*') ? 'active' : '' }}">
    <a href="{!! route('decisions.index') !!}"><i class="fa fa-edit"></i><span>Decisions</span></a>
</li> -->

