<!-- Respondent Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('respondent_id', 'Respondent Id:') !!}
    {!! Form::select('respondent_id', ['s' => 's'], null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('decisions.index') !!}" class="btn btn-default">Cancel</a>
</div>
