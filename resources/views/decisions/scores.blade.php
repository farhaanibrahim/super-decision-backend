@extends('layouts.app')
@section('css')
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            {{ $title }}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            @foreach($keys as $key)
                            <th>{{ $key }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($keys as $k1)
                        <tr>
                            @foreach($keys as $k=>$k2)
                            @if($k == 0)
                            <th>{{ $k1 }}</th>
                            @endif
                            @if($k1 == $k2)
                                <td>1</td>
                            @else
                                @if(array_key_exists($k1, $data) && array_key_exists($k2, $data[$k1]))
                                <td>{{ pow($data[$k1][$k2], 1/$total) }}</td>
                                @else
                                <td>{{ pow(floatval(1)/$data[$k2][$k1], 1/$total) }}</td>
                                @endif
                            @endif

                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('.table').DataTable({
            "ordering": false,
            "scrollX": true,
            "pageLength": 3000
        });
    });
</script>
@endsection
