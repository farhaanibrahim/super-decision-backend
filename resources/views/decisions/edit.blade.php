@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Decision
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($decision, ['route' => ['decisions.update', $decision->id], 'method' => 'patch']) !!}

                        @include('decisions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection