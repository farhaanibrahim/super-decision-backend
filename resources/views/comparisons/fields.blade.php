<!-- Decision Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('decision_id', 'Decision Id:') !!}
    {!! Form::select('decision_id', ['s' => 's'], null, ['class' => 'form-control']) !!}
</div>

<!-- Left Field -->
<div class="form-group col-sm-6">
    {!! Form::label('left', 'Left:') !!}
    {!! Form::text('left', null, ['class' => 'form-control']) !!}
</div>

<!-- Right Field -->
<div class="form-group col-sm-6">
    {!! Form::label('right', 'Right:') !!}
    {!! Form::text('right', null, ['class' => 'form-control']) !!}
</div>

<!-- Score Field -->
<div class="form-group col-sm-6">
    {!! Form::label('score', 'Score:') !!}
    {!! Form::text('score', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('comparisons.index') !!}" class="btn btn-default">Cancel</a>
</div>
