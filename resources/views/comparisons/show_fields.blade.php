<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $comparison->id !!}</p>
</div>

<!-- Decision Id Field -->
<div class="form-group">
    {!! Form::label('decision_id', 'Decision Id:') !!}
    <p>{!! $comparison->decision_id !!}</p>
</div>

<!-- Left Field -->
<div class="form-group">
    {!! Form::label('left', 'Left:') !!}
    <p>{!! $comparison->left !!}</p>
</div>

<!-- Right Field -->
<div class="form-group">
    {!! Form::label('right', 'Right:') !!}
    <p>{!! $comparison->right !!}</p>
</div>

<!-- Score Field -->
<div class="form-group">
    {!! Form::label('score', 'Score:') !!}
    <p>{!! $comparison->score !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $comparison->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $comparison->updated_at !!}</p>
</div>

