<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            [
                'Admin', 'Travel Agent', 'Traveler', 'Expertise'
            ]
        );

        $countArray = count($roles[0]);

        for ($i=0; $i < $countArray; $i++) { 
            DB::table('roles')->insert([
                'role_name'     => $roles[0][$i],
                'created_at'    => Carbon::now('Asia/Jakarta'),
                'updated_at'    => Carbon::now('Asia/Jakarta')
            ]);
        }
    }
}
