<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            [
                'Admin', 'Travel Agent', 'Traveller', 'Expertise'
            ],
            [
                '081314118157', '081314118157', '081314118157', '081314118157', 
            ],
            [
                'admin@ugt.com', 'travel.agent@ugt.com', 'traveller@ugt.com', 'expertise@ugt.com'
            ],
            [
                '1', '2', '3', '4'
            ]
        );

        $countArray = count($users[0]);

        for ($i=0; $i < $countArray; $i++) { 
            DB::table('users')->insert([
                'name' => $users[0][$i],
                'phone'=> $users[1][$i],
                'email'=> $users[2][$i],
                'password' => Hash::make('123456'),
                'role_id'   => $users[3][$i],
                'created_at' => Carbon::now('Asia/Jakarta'),
                'updated_at' => Carbon::now('Asia/Jakarta'),
            ]);
        }
    }
}
