<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = array(
            [
                'Bandung', 'Yogyakarta', 'Banyuwangi', 'Malaka', 'Bali'
            ]
        );

        $countArray = count($cities[0]);

        for ($i=0; $i < $countArray; $i++) { 
            DB::table('cities')->insert([
                'city_name' => $cities[0][$i],
            ]);
        }
    }
}
