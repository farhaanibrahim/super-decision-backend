<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentInfos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('respondent_id')->unsigned()->nullable();
            $table->string('gender')->nullable();
            $table->integer('age')->nullable();
            $table->timestamps();

            $table->foreign('respondent_id')->references('id')->on('respondents')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondent_infos');
    }
}
