<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Comparison
 * @package App\Models
 * @version July 25, 2019, 2:43 pm UTC
 *
 * @property \App\Models\Decision decision
 * @property integer decision_id
 * @property string left
 * @property string right
 * @property float score
 */
class Comparison extends Model
{
    use SoftDeletes;

    public $table = 'comparisons';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'decision_id',
        'left',
        'right',
        'score'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'decision_id' => 'integer',
        'left' => 'string',
        'right' => 'string',
        'score' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function decision()
    {
        return $this->belongsTo(\App\Models\Decision::class, 'decision_id', 'id');
    }
}
