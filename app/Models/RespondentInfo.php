<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RespondentInfo
 * @package App\Models
 * @version July 25, 2019, 2:43 pm UTC
 *
 * @property \App\Models\Respondent respondent
 * @property integer respondent_id
 * @property string gender
 * @property integer age
 */
class RespondentInfo extends Model
{
    use SoftDeletes;

    public $table = 'respondent_infos';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'respondent_id',
        'gender',
        'age'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'respondent_id' => 'integer',
        'gender' => 'string',
        'age' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function respondent()
    {
        return $this->belongsTo(\App\Models\Respondent::class, 'respondent_id', 'id');
    }
}
