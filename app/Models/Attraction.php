<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attraction extends Model
{
    public $table = 'attractions';

    public $fillable = [
        'user_id',
        'attraction_name',
        'city_id'
    ];

    protected $casts = [
        'user_id'   => 'integer',
        'attraction_name'   => 'string',
        'city_id'   => 'integer'
    ];

    public function users()
    {
        return $this->belongsTo(\App\Models\Users::class,'user_id', 'id');
    }

    public function cities()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
    }
}
