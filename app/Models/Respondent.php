<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Respondent
 * @package App\Models
 * @version July 25, 2019, 2:43 pm UTC
 *
 * @property string name
 * @property string phone
 * @property string respondent_type
 * @property integer traveling_budget
 * @property string start_date
 * @property string end_date
 */
class Respondent extends Model
{
    use SoftDeletes;

    public $table = 'respondents';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'number_of_person',
        'traveling_budget',
        'day',
        'night',
        'start_date',
        'end_date',
        'city'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'xid' => 'string',
        'name' => 'string',
        'phone' => 'string',
        'respondent_type' => 'string',
        'number_of_person' => 'integer',
        'traveling_budget' => 'integer',
        'day' => 'integer',
        'night' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date',
        'city' => 'city'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function formattedBudget() {
        return \App\Helpers\CurrencyHelper::formatCurrency($this->traveling_budget);
    }

    public function users()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function respondentInfos()
    {
        return $this->hasMany(\App\Models\RespondentInfo::class, 'respondent_id', 'id');
    }

    public function decisions()
    {
        return $this->hasMany(\App\Models\Decision::class, 'respondent_id', 'id');
    }

    /*public function duration() {
        $diff = strtotime($this->end_date) - strtotime($this->start_date);
        return (\round($diff / (60 * 60 * 24)) + 1);
    }*/

    public function duration() {
        return $this->day." day(s) ". $this->night." night(s)";
    }

    public function period() {
        return \App\Helpers\DateHelper::formatDate($this->start_date). ' - '. \App\Helpers\DateHelper::formatDate($this->end_date);
    }
}
