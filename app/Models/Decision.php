<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Decision
 * @package App\Models
 * @version July 25, 2019, 2:43 pm UTC
 *
 * @property \App\Models\Respondent respondent
 * @property integer respondent_id
 * @property string title
 */
class Decision extends Model
{
    use SoftDeletes;

    public $table = 'decisions';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'respondent_id',
        'title'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'respondent_id' => 'integer',
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function respondent()
    {
        return $this->belongsTo(\App\Models\Respondent::class, 'respondent_id', 'id');
    }

    public function comparisons()
    {
        return $this->hasMany(\App\Models\Comparison::class, 'decision_id', 'id');
    }
}
