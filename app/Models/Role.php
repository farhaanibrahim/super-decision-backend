<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $table = 'roles';

    protected $fillable = [
        'role_name'
    ];

    protected $casts = [
        'role_name' => 'string'
    ];

    public function users()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }
}
