<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $table = 'users';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'phone',
        'email',
        'email_verified_at',
        'password',
        'role_id',
        'remember_token'
    ];

    protected $casts = [
        'name'  => 'string',
        'phone' => 'string',
        'email',
        'email_verified_at',
        'password',
        'role_id',
        'remember_token'
    ];

    public function roles()
    {
        return $this->hasOne(\App\Models\Role::class, 'role_id', 'id');
    }

    public function attractions()
    {
        return $this->hasMany(\App\Models\Attraction::class, 'user_id', 'id');
    }
}
