<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $table = 'cities';

    public $fillable = [
        'city_name',
    ];

    protected $casts = [
        'city_name'   => 'string',
    ];

    public function attractions()
    {
        return $this->hasMany(\App\Model\Attraction::class, 'city_id', 'id');
    }
}
