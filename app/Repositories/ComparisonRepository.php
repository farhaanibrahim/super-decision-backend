<?php

namespace App\Repositories;

use App\Models\Comparison;
use App\Repositories\BaseRepository;

/**
 * Class ComparisonRepository
 * @package App\Repositories
 * @version July 25, 2019, 2:43 pm UTC
*/

class ComparisonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Comparison::class;
    }
}
