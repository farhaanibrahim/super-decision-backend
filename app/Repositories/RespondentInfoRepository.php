<?php

namespace App\Repositories;

use App\Models\RespondentInfo;
use App\Repositories\BaseRepository;

/**
 * Class RespondentInfoRepository
 * @package App\Repositories
 * @version July 25, 2019, 2:43 pm UTC
*/

class RespondentInfoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RespondentInfo::class;
    }
}
