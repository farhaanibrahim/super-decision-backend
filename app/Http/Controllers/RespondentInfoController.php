<?php

namespace App\Http\Controllers;

use App\DataTables\RespondentInfoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRespondentInfoRequest;
use App\Http\Requests\UpdateRespondentInfoRequest;
use App\Repositories\RespondentInfoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RespondentInfoController extends AppBaseController
{
    /** @var  RespondentInfoRepository */
    private $respondentInfoRepository;

    public function __construct(RespondentInfoRepository $respondentInfoRepo)
    {
        $this->respondentInfoRepository = $respondentInfoRepo;
    }

    /**
     * Display a listing of the RespondentInfo.
     *
     * @param RespondentInfoDataTable $respondentInfoDataTable
     * @return Response
     */
    public function index(RespondentInfoDataTable $respondentInfoDataTable)
    {
        return $respondentInfoDataTable->render('respondent_infos.index');
    }

    /**
     * Show the form for creating a new RespondentInfo.
     *
     * @return Response
     */
    public function create()
    {
        return view('respondent_infos.create');
    }

    /**
     * Store a newly created RespondentInfo in storage.
     *
     * @param CreateRespondentInfoRequest $request
     *
     * @return Response
     */
    public function store(CreateRespondentInfoRequest $request)
    {
        $input = $request->all();

        $respondentInfo = $this->respondentInfoRepository->create($input);

        Flash::success('Respondent Info saved successfully.');

        return redirect(route('respondentInfos.index'));
    }

    /**
     * Display the specified RespondentInfo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $respondentInfo = $this->respondentInfoRepository->find($id);

        if (empty($respondentInfo)) {
            Flash::error('Respondent Info not found');

            return redirect(route('respondentInfos.index'));
        }

        return view('respondent_infos.show')->with('respondentInfo', $respondentInfo);
    }

    /**
     * Show the form for editing the specified RespondentInfo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $respondentInfo = $this->respondentInfoRepository->find($id);

        if (empty($respondentInfo)) {
            Flash::error('Respondent Info not found');

            return redirect(route('respondentInfos.index'));
        }

        return view('respondent_infos.edit')->with('respondentInfo', $respondentInfo);
    }

    /**
     * Update the specified RespondentInfo in storage.
     *
     * @param  int              $id
     * @param UpdateRespondentInfoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRespondentInfoRequest $request)
    {
        $respondentInfo = $this->respondentInfoRepository->find($id);

        if (empty($respondentInfo)) {
            Flash::error('Respondent Info not found');

            return redirect(route('respondentInfos.index'));
        }

        $respondentInfo = $this->respondentInfoRepository->update($request->all(), $id);

        Flash::success('Respondent Info updated successfully.');

        return redirect(route('respondentInfos.index'));
    }

    /**
     * Remove the specified RespondentInfo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $respondentInfo = $this->respondentInfoRepository->find($id);

        if (empty($respondentInfo)) {
            Flash::error('Respondent Info not found');

            return redirect(route('respondentInfos.index'));
        }

        $this->respondentInfoRepository->delete($id);

        Flash::success('Respondent Info deleted successfully.');

        return redirect(route('respondentInfos.index'));
    }
}
