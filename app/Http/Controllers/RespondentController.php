<?php

namespace App\Http\Controllers;

use App\DataTables\RespondentDataTable;
use App\DataTables\TravelerDataTable;
use App\DataTables\ExpertiseDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRespondentRequest;
use App\Http\Requests\UpdateRespondentRequest;
use App\Repositories\RespondentRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RespondentController extends AppBaseController
{
    /** @var  RespondentRepository */
    private $respondentRepository;

    public function __construct(RespondentRepository $respondentRepo)
    {
        $this->respondentRepository = $respondentRepo;
    }

    /**
     * Display a listing of the Respondent.
     *
     * @param RespondentDataTable $respondentDataTable
     * @return Response
     */
    public function index(RespondentDataTable $respondentDataTable)
    {
        return $respondentDataTable->render('respondents.index');
    }

    public function travelers(TravelerDataTable $travelerDataTable)
    {
        return $travelerDataTable->render('respondents.index');
    }

    public function expertises(ExpertiseDataTable $expertiseDataTable)
    {
        return $expertiseDataTable->render('respondents.index');
    }

    /**
     * Show the form for creating a new Respondent.
     *
     * @return Response
     */
    public function create()
    {
        $count = \App\Models\Respondent::where('respondent_type', 'Expertise')->count() + 1;
        $xid = "EXP{$count}";
        return view('respondents.create', compact('xid'));
    }

    /**
     * Store a newly created Respondent in storage.
     *
     * @param CreateRespondentRequest $request
     *
     * @return Response
     */
    public function store(CreateRespondentRequest $request)
    {
        $input = $request->all();

        $respondent = $this->respondentRepository->create($input);

        Flash::success('Expertise saved successfully.');

        return redirect(route('respondents.expertises'));
    }

    /**
     * Display the specified Respondent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $respondent = $this->respondentRepository->find($id);

        if (empty($respondent)) {
            Flash::error('Respondent not found');

            return redirect(route('respondents.index'));
        }

        return view('respondents.show')->with('respondent', $respondent);
    }

    /**
     * Show the form for editing the specified Respondent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $respondent = $this->respondentRepository->find($id);

        if (empty($respondent)) {
            Flash::error('Respondent not found');

            return redirect(route('respondents.index'));
        }

        $xid = $respondent->xid;

        return view('respondents.edit', compact('xid'))->with('respondent', $respondent);
    }

    /**
     * Update the specified Respondent in storage.
     *
     * @param  int              $id
     * @param UpdateRespondentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRespondentRequest $request)
    {
        $respondent = $this->respondentRepository->find($id);

        if (empty($respondent)) {
            Flash::error('Respondent not found');

            return redirect(route('respondents.index'));
        }

        $respondent = $this->respondentRepository->update($request->all(), $id);

        Flash::success('Expertise updated successfully.');

        return redirect(route('respondents.expertises'));
    }

    /**
     * Remove the specified Respondent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $respondent = $this->respondentRepository->find($id);

        if (empty($respondent)) {
            Flash::error('Respondent not found');

            return redirect(route('respondents.index'));
        }

        $this->respondentRepository->delete($id);

        Flash::success('Expertise deleted successfully.');

        return redirect(route('respondents.expertises'));
    }

    public function scores()
    {
        $titles = ['Geophysical-Landscape-Aesthetic', 'Ecological-Biological', 'Cultural-Historical', 'Recreational'];
        return view('respondents.scores', compact('titles'));
    }
}
