<?php

namespace App\Http\Controllers;

use App\DataTables\DecisionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDecisionRequest;
use App\Http\Requests\UpdateDecisionRequest;
use App\Repositories\DecisionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DecisionController extends AppBaseController
{
    /** @var  DecisionRepository */
    private $decisionRepository;

    public function __construct(DecisionRepository $decisionRepo)
    {
        $this->decisionRepository = $decisionRepo;
    }

    /**
     * Display a listing of the Decision.
     *
     * @param DecisionDataTable $decisionDataTable
     * @return Response
     */
    public function index(DecisionDataTable $decisionDataTable)
    {
        return $decisionDataTable->render('decisions.index');
    }

    /**
     * Show the form for creating a new Decision.
     *
     * @return Response
     */
    public function create()
    {
        return view('decisions.create');
    }

    /**
     * Store a newly created Decision in storage.
     *
     * @param CreateDecisionRequest $request
     *
     * @return Response
     */
    public function store(CreateDecisionRequest $request)
    {
        $input = $request->all();

        $decision = $this->decisionRepository->create($input);

        Flash::success('Decision saved successfully.');

        return redirect(route('decisions.index'));
    }

    /**
     * Display the specified Decision.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $decision = $this->decisionRepository->find($id);

        if (empty($decision)) {
            Flash::error('Decision not found');

            return redirect(route('decisions.index'));
        }

        $keys = [];
        $data = [];

        foreach($decision->comparisons as $comparison) {
            if(!in_array($comparison->left, $keys)) {
                $keys[] = $comparison->left;
            }

            if(!in_array($comparison->right, $keys)) {
                $keys[] = $comparison->right;
            }

            if(!array_key_exists($comparison->left, $data)) {
                $data[$comparison->left] = [];
            }

            $data[$comparison->left][$comparison->right] = $comparison->score;
        }

        return view('decisions.show', compact('keys', 'data'))->with('decision', $decision);
    }

    /**
     * Show the form for editing the specified Decision.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $decision = $this->decisionRepository->find($id);

        if (empty($decision)) {
            Flash::error('Decision not found');

            return redirect(route('decisions.index'));
        }

        return view('decisions.edit')->with('decision', $decision);
    }

    /**
     * Update the specified Decision in storage.
     *
     * @param  int              $id
     * @param UpdateDecisionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDecisionRequest $request)
    {
        $decision = $this->decisionRepository->find($id);

        if (empty($decision)) {
            Flash::error('Decision not found');

            return redirect(route('decisions.index'));
        }

        $decision = $this->decisionRepository->update($request->all(), $id);

        Flash::success('Decision updated successfully.');

        return redirect(route('decisions.index'));
    }

    /**
     * Remove the specified Decision from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $decision = $this->decisionRepository->find($id);

        if (empty($decision)) {
            Flash::error('Decision not found');

            return redirect(route('decisions.index'));
        }

        $this->decisionRepository->delete($id);

        Flash::success('Decision deleted successfully.');

        return redirect(route('decisions.index'));
    }

    public function scores($title) {
        $title = urldecode($title);
        $keys = [];
        $data = [];

        $decisions = \App\Models\Decision::where('title', $title)
            ->whereHas('respondent', function ($query) {
                $query->where('respondent_type', "Expertise");
            })
            ->get();
        $total = $decisions->count();

        foreach($decisions as $decision) {
            foreach($decision->comparisons as $comparison) {
                if(!in_array($comparison->left, $keys)) {
                    $keys[] = $comparison->left;
                }

                if(!in_array($comparison->right, $keys)) {
                    $keys[] = $comparison->right;
                }

                if(!array_key_exists($comparison->left, $data)) {
                    $data[$comparison->left] = [];
                }

                if(empty($data[$comparison->left][$comparison->right])) {
                    $data[$comparison->left][$comparison->right] = $comparison->score;
                }
                else {
                    $data[$comparison->left][$comparison->right] *= $comparison->score;
                }
            }
        }

        //var_dump($data); die();

        return view('decisions.scores', compact('title', 'keys', 'data', 'total'));
    }

    public function scores2($title) {
        $title = urldecode($title);
        $keys = [];
        $data = [];

        $decisions = \App\Models\Decision::where('title', $title)
            ->whereHas('respondent', function ($query) {
                $query->where('respondent_type', "Expertise");
            })
            ->get();
        $total = $decisions->count();

        foreach($decisions as $decision) {
            foreach($decision->comparisons as $comparison) {
                if(!in_array($comparison->left, $keys)) {
                    $keys[] = $comparison->left;
                }

                if(!in_array($comparison->right, $keys)) {
                    $keys[] = $comparison->right;
                }

                if(!array_key_exists($comparison->left, $data)) {
                    $data[$comparison->left] = [];
                }

                if(empty($data[$comparison->left][$comparison->right])) {
                    $data[$comparison->left][$comparison->right] = $comparison->score;
                }
                else {
                    $data[$comparison->left][$comparison->right] *= $comparison->score;
                }
            }
        }

        return view('decisions.scores2', compact('title', 'keys', 'data', 'total'));
    }
}
