<?php

namespace App\Http\Controllers;

use App\DataTables\ComparisonDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateComparisonRequest;
use App\Http\Requests\UpdateComparisonRequest;
use App\Repositories\ComparisonRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ComparisonController extends AppBaseController
{
    /** @var  ComparisonRepository */
    private $comparisonRepository;

    public function __construct(ComparisonRepository $comparisonRepo)
    {
        $this->comparisonRepository = $comparisonRepo;
    }

    /**
     * Display a listing of the Comparison.
     *
     * @param ComparisonDataTable $comparisonDataTable
     * @return Response
     */
    public function index(ComparisonDataTable $comparisonDataTable)
    {
        return $comparisonDataTable->render('comparisons.index');
    }

    /**
     * Show the form for creating a new Comparison.
     *
     * @return Response
     */
    public function create()
    {
        return view('comparisons.create');
    }

    /**
     * Store a newly created Comparison in storage.
     *
     * @param CreateComparisonRequest $request
     *
     * @return Response
     */
    public function store(CreateComparisonRequest $request)
    {
        $input = $request->all();

        $comparison = $this->comparisonRepository->create($input);

        Flash::success('Comparison saved successfully.');

        return redirect(route('comparisons.index'));
    }

    /**
     * Display the specified Comparison.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $comparison = $this->comparisonRepository->find($id);

        if (empty($comparison)) {
            Flash::error('Comparison not found');

            return redirect(route('comparisons.index'));
        }

        return view('comparisons.show')->with('comparison', $comparison);
    }

    /**
     * Show the form for editing the specified Comparison.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $comparison = $this->comparisonRepository->find($id);

        if (empty($comparison)) {
            Flash::error('Comparison not found');

            return redirect(route('comparisons.index'));
        }

        return view('comparisons.edit')->with('comparison', $comparison);
    }

    /**
     * Update the specified Comparison in storage.
     *
     * @param  int              $id
     * @param UpdateComparisonRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateComparisonRequest $request)
    {
        $comparison = $this->comparisonRepository->find($id);

        if (empty($comparison)) {
            Flash::error('Comparison not found');

            return redirect(route('comparisons.index'));
        }

        $comparison = $this->comparisonRepository->update($request->all(), $id);

        Flash::success('Comparison updated successfully.');

        return redirect(route('comparisons.index'));
    }

    /**
     * Remove the specified Comparison from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $comparison = $this->comparisonRepository->find($id);

        if (empty($comparison)) {
            Flash::error('Comparison not found');

            return redirect(route('comparisons.index'));
        }

        $this->comparisonRepository->delete($id);

        Flash::success('Comparison deleted successfully.');

        return redirect(route('comparisons.index'));
    }
}
