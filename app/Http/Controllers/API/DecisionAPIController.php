<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDecisionAPIRequest;
use App\Http\Requests\API\UpdateDecisionAPIRequest;
use App\Models\Decision;
use App\Repositories\DecisionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DecisionController
 * @package App\Http\Controllers\API
 */

class DecisionAPIController extends AppBaseController
{
    /** @var  DecisionRepository */
    private $decisionRepository;

    public function __construct(DecisionRepository $decisionRepo)
    {
        $this->decisionRepository = $decisionRepo;
    }

    /**
     * Display a listing of the Decision.
     * GET|HEAD /decisions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $decisions = $this->decisionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($decisions->toArray(), 'Decisions retrieved successfully');
    }

    /**
     * Store a newly created Decision in storage.
     * POST /decisions
     *
     * @param CreateDecisionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDecisionAPIRequest $request)
    {
        $input = $request->all();

        $decision = $this->decisionRepository->create($input);

        return $this->sendResponse($decision->toArray(), 'Decision saved successfully');
    }

    /**
     * Display the specified Decision.
     * GET|HEAD /decisions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Decision $decision */
        $decision = $this->decisionRepository->find($id);

        if (empty($decision)) {
            return $this->sendError('Decision not found');
        }

        return $this->sendResponse($decision->toArray(), 'Decision retrieved successfully');
    }

    /**
     * Update the specified Decision in storage.
     * PUT/PATCH /decisions/{id}
     *
     * @param int $id
     * @param UpdateDecisionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDecisionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Decision $decision */
        $decision = $this->decisionRepository->find($id);

        if (empty($decision)) {
            return $this->sendError('Decision not found');
        }

        $decision = $this->decisionRepository->update($input, $id);

        return $this->sendResponse($decision->toArray(), 'Decision updated successfully');
    }

    /**
     * Remove the specified Decision from storage.
     * DELETE /decisions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Decision $decision */
        $decision = $this->decisionRepository->find($id);

        if (empty($decision)) {
            return $this->sendError('Decision not found');
        }

        $decision->delete();

        return $this->sendResponse($id, 'Decision deleted successfully');
    }
}
