<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateComparisonAPIRequest;
use App\Http\Requests\API\UpdateComparisonAPIRequest;
use App\Models\Comparison;
use App\Repositories\ComparisonRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ComparisonController
 * @package App\Http\Controllers\API
 */

class ComparisonAPIController extends AppBaseController
{
    /** @var  ComparisonRepository */
    private $comparisonRepository;

    public function __construct(ComparisonRepository $comparisonRepo)
    {
        $this->comparisonRepository = $comparisonRepo;
    }

    /**
     * Display a listing of the Comparison.
     * GET|HEAD /comparisons
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $comparisons = $this->comparisonRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($comparisons->toArray(), 'Comparisons retrieved successfully');
    }

    /**
     * Store a newly created Comparison in storage.
     * POST /comparisons
     *
     * @param CreateComparisonAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateComparisonAPIRequest $request)
    {
        $input = $request->all();

        $comparison = $this->comparisonRepository->create($input);

        return $this->sendResponse($comparison->toArray(), 'Comparison saved successfully');
    }

    /**
     * Display the specified Comparison.
     * GET|HEAD /comparisons/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Comparison $comparison */
        $comparison = $this->comparisonRepository->find($id);

        if (empty($comparison)) {
            return $this->sendError('Comparison not found');
        }

        return $this->sendResponse($comparison->toArray(), 'Comparison retrieved successfully');
    }

    /**
     * Update the specified Comparison in storage.
     * PUT/PATCH /comparisons/{id}
     *
     * @param int $id
     * @param UpdateComparisonAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateComparisonAPIRequest $request)
    {
        $input = $request->all();

        /** @var Comparison $comparison */
        $comparison = $this->comparisonRepository->find($id);

        if (empty($comparison)) {
            return $this->sendError('Comparison not found');
        }

        $comparison = $this->comparisonRepository->update($input, $id);

        return $this->sendResponse($comparison->toArray(), 'Comparison updated successfully');
    }

    /**
     * Remove the specified Comparison from storage.
     * DELETE /comparisons/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Comparison $comparison */
        $comparison = $this->comparisonRepository->find($id);

        if (empty($comparison)) {
            return $this->sendError('Comparison not found');
        }

        $comparison->delete();

        return $this->sendResponse($id, 'Comparison deleted successfully');
    }
}
