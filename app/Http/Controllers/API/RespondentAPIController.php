<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRespondentAPIRequest;
use App\Http\Requests\API\UpdateRespondentAPIRequest;
use App\Http\Requests\API\SubmitRespondentAPIRequest;
use App\Models\Respondent;
use App\Repositories\RespondentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Input;
use DB;

/**
 * Class RespondentController
 * @package App\Http\Controllers\API
 */

class RespondentAPIController extends AppBaseController
{
    /** @var  RespondentRepository */
    private $respondentRepository;

    public function __construct(RespondentRepository $respondentRepo)
    {
        $this->respondentRepository = $respondentRepo;
    }

    /**
     * Display a listing of the Respondent.
     * GET|HEAD /respondents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $respondents = $this->respondentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($respondents->toArray(), 'Respondents retrieved successfully');
    }

    /**
     * Store a newly created Respondent in storage.
     * POST /respondents
     *
     * @param CreateRespondentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRespondentAPIRequest $request)
    {
        $input = $request->all();

        $respondent = $this->respondentRepository->create($input);

        return $this->sendResponse($respondent->toArray(), 'Respondent saved successfully');
    }

    /**
     * Display the specified Respondent.
     * GET|HEAD /respondents/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Respondent $respondent */
        $respondent = $this->respondentRepository->find($id);

        if (empty($respondent)) {
            return $this->sendError('Respondent not found');
        }

        return $this->sendResponse($respondent->toArray(), 'Respondent retrieved successfully');
    }

    /**
     * Update the specified Respondent in storage.
     * PUT/PATCH /respondents/{id}
     *
     * @param int $id
     * @param UpdateRespondentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRespondentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Respondent $respondent */
        $respondent = $this->respondentRepository->find($id);

        if (empty($respondent)) {
            return $this->sendError('Respondent not found');
        }

        $respondent = $this->respondentRepository->update($input, $id);

        return $this->sendResponse($respondent->toArray(), 'Respondent updated successfully');
    }

    /**
     * Remove the specified Respondent from storage.
     * DELETE /respondents/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Respondent $respondent */
        $respondent = $this->respondentRepository->find($id);

        if (empty($respondent)) {
            return $this->sendError('Respondent not found');
        }

        $respondent->delete();

        return $this->sendResponse($id, 'Respondent deleted successfully');
    }

    public function submit() {
        DB::transaction(function () {
            $xid = trim(Input::get('xid'));
            $name = trim(Input::get('name'));
            $phone = trim(Input::get('phone'));
            $stype = trim(Input::get('stype'));
            $number_of_person = trim(Input::get('number_of_person'));
            $day = trim(Input::get('day'));
            $night = trim(Input::get('night'));
            $traveling_budget = trim(Input::get('traveling_budget'));
            $traveling_start_date = trim(Input::get('traveling_start_date'));
            $traveling_end_date = trim(Input::get('traveling_end_date'));
            $gender_and_age = trim(Input::get('gender_and_age'));
            $decisions = trim(Input::get('decisions'));
            $city = trim(Input::get('city'));

            $respondent = null;

            if($stype == 'Expertise') {
                $respondent = \App\Models\Respondent::where('xid', $xid)->first();

                if(empty($respondent)) {
                    return $this->sendError('Expertise not found');
                }
            }
            else {
                $respondent = new \App\Models\Respondent;
                $respondent->name = $name;
                $respondent->phone = $phone;
                $respondent->respondent_type = $stype;
                $respondent->number_of_person = $number_of_person;
                $respondent->day = $day;
                $respondent->night = $night;
                $respondent->traveling_budget = $traveling_budget;
                $respondent->start_date = $traveling_start_date;
                $respondent->end_date = $traveling_end_date;
                $respondent->city = $city;
                $respondent->save();

                $gender_and_age_array = json_decode($gender_and_age, true);

                foreach($gender_and_age_array as $item) {
                    $respondentInfo = new \App\Models\RespondentInfo;
                    $respondentInfo->respondent_id = $respondent->id;
                    $respondentInfo->gender = $item['gender'];
                    $respondentInfo->age = $item['age'];
                    $respondentInfo->save();
                }
            }

            \App\Models\Comparison::whereHas('decision', function ($query) use($respondent) {
                $query->where('respondent_id', $respondent->id);
            })->delete();

            \App\Models\Decision::where('respondent_id', $respondent->id)->delete();

            $decision_array = json_decode($decisions, true);

            foreach($decision_array as $item) {
                $decision = new \App\Models\Decision;
                $decision->respondent_id = $respondent->id;
                $decision->title = trim(preg_replace('/\s+/', '-', $item['root']));
                $decision->save();

                $comparisons = $item['comparisons'];

                foreach($comparisons as $c) {
                    $score = $this->convertScore2($c['score']);

                    $comparison = new \App\Models\Comparison;
                    $comparison->decision_id = $decision->id;
                    $comparison->left = $c['left'];
                    $comparison->right = $c['right'];
                    $comparison->score = $score;
                    $comparison->save();
                }
            }
        });

        return $this->sendResponse([], 'Respondents retrieved successfully');
    }

    private function convertScore($score) {
        $sc = $score % 10;

        if($score == 10) {
            $sc = 1;
        }
        else if($score < 10) {
            $sc = 10 - ($sc + 1);
        }
        else if($score > 10) {
            $sc = floatval(1)/($sc + 1);
        }

        return $sc;
    }

    private function convertScore2($score) {
        if($score < 10) {
            return 10 - $score;
        }
        else {
            return floatval(1)/($score - 8);
        }
    }

    public function check() {
        $xid = Input::get('xid');
        $respondent = \App\Models\Respondent::where('xid', $xid)->first();

        if(empty($respondent)) {
            return $this->sendError('Expertise ID not found');
        }

        return $this->sendResponse([], 'Expertise exists');
    }
}
