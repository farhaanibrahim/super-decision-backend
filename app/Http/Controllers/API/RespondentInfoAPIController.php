<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRespondentInfoAPIRequest;
use App\Http\Requests\API\UpdateRespondentInfoAPIRequest;
use App\Models\RespondentInfo;
use App\Repositories\RespondentInfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RespondentInfoController
 * @package App\Http\Controllers\API
 */

class RespondentInfoAPIController extends AppBaseController
{
    /** @var  RespondentInfoRepository */
    private $respondentInfoRepository;

    public function __construct(RespondentInfoRepository $respondentInfoRepo)
    {
        $this->respondentInfoRepository = $respondentInfoRepo;
    }

    /**
     * Display a listing of the RespondentInfo.
     * GET|HEAD /respondentInfos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $respondentInfos = $this->respondentInfoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($respondentInfos->toArray(), 'Respondent Infos retrieved successfully');
    }

    /**
     * Store a newly created RespondentInfo in storage.
     * POST /respondentInfos
     *
     * @param CreateRespondentInfoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRespondentInfoAPIRequest $request)
    {
        $input = $request->all();

        $respondentInfo = $this->respondentInfoRepository->create($input);

        return $this->sendResponse($respondentInfo->toArray(), 'Respondent Info saved successfully');
    }

    /**
     * Display the specified RespondentInfo.
     * GET|HEAD /respondentInfos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RespondentInfo $respondentInfo */
        $respondentInfo = $this->respondentInfoRepository->find($id);

        if (empty($respondentInfo)) {
            return $this->sendError('Respondent Info not found');
        }

        return $this->sendResponse($respondentInfo->toArray(), 'Respondent Info retrieved successfully');
    }

    /**
     * Update the specified RespondentInfo in storage.
     * PUT/PATCH /respondentInfos/{id}
     *
     * @param int $id
     * @param UpdateRespondentInfoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRespondentInfoAPIRequest $request)
    {
        $input = $request->all();

        /** @var RespondentInfo $respondentInfo */
        $respondentInfo = $this->respondentInfoRepository->find($id);

        if (empty($respondentInfo)) {
            return $this->sendError('Respondent Info not found');
        }

        $respondentInfo = $this->respondentInfoRepository->update($input, $id);

        return $this->sendResponse($respondentInfo->toArray(), 'RespondentInfo updated successfully');
    }

    /**
     * Remove the specified RespondentInfo from storage.
     * DELETE /respondentInfos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RespondentInfo $respondentInfo */
        $respondentInfo = $this->respondentInfoRepository->find($id);

        if (empty($respondentInfo)) {
            return $this->sendError('Respondent Info not found');
        }

        $respondentInfo->delete();

        return $this->sendResponse($id, 'Respondent Info deleted successfully');
    }
}
