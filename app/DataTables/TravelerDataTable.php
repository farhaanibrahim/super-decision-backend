<?php

namespace App\DataTables;

use App\Models\Respondent;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class TravelerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'respondents.datatables_actions')
            ->addColumn('duration', function($respondent){
                return $respondent->duration();
            })
            ->addColumn('traveling_budget_name', function($respondent){
                return $respondent->formattedBudget();
            })
            ->addColumn('period_name', function($respondent){
                return $respondent->period();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Respondent $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Respondent $model)
    {
        return $model->newQuery()->where('respondent_type', 'Traveller');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    /*['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],*/
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'updated_at' => ['searchable' => false, 'visible' => false],
            'name' => ['searchable' => true],
            'phone' => ['searchable' => true, 'class' => 'text-center'],
            'number_of_person' => ['searchable' => false, 'title' => 'Person', 'class' => 'text-center'],
            'traveling_budget_name' => ['searchable' => false, 'class' => 'text-center', 'title' => 'Budget'],
            'duration' => ['searchable' => false, 'class' => 'text-center', 'title' => 'Duration (Day)'],
            'period_name' => ['searchable' => false, 'title' => 'Period', 'class' => 'text-center']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'respondentsdatatable_' . time();
    }
}
